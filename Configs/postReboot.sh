#!/bin/bash
# Cups
gcp-cups-connector-util init
# Idea
idea-ultimate
# Studio
android
# Fingerprint
fingerprint-gui
# Rclone
echo "Copy the contents of this file to"
echo "     ~/.config/rclone/rclone.conf"
echo "     https://gist.github.com/billwi/7b3982fcd0be1b1bb1199fc1732e80fc"
# Steam
steam
